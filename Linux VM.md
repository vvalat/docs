## Linux on  a VM ##
Based on Oracle Virtual Box

 1. Manage BIOS settings so that virtualization is enabled
 
 2. Adapt VM configuration to guest OS requirements
 
 3. Make 3 partitions ( Root | Swap | Home)
 
 4. Install selected distribution

 5. Some pointers:
	 1. Make full use of bidirectional clipboard
	 2. Use an american locale
 6. Install basic packages:
	 - build-essential
	 - dkms
	 - gcc
	 - linux-headers-$(uname -r)
 7. Mount Guest Additions CD Image and input these commands:
 
    	mkdir /tmp/vboxadd
    	cp -r /media/cdrom0/* /tmp/vboxadd
    	cd /tmp/vboxadd
    	su
    	./VBoxLinuxAdditions.run
    	adduser <username to access shared folders> vboxsf
    	
