## Managing file permissions ##
Basic usage (set read, write, execute for all):

    chmod +[r,w,x] <file/folder>

Custom usage:

    chmod <n1><n2><n3> <file/folder>

Where n1, n2 and n3 are numbers in octal base, respectively for owner, group and public.

Each number is derived from a bitwise operator:

> No rights > 0
> Execute > 1
> Write > 2
> Read > 4

So, a read/write flag is 4 + 2 = 6 ; a readonly is 4 and so forth...

A few examples:
 - 755 (all rights granted to owner, read and execute for others)
 - 644 (read/write to owner, read only for others)
 - 500 (only the owner can read and execute)
 
In order to get octal code for a specific file/directory:

    stat -c "%a" <name>

In order to change the owner:

    chown user:group <name>