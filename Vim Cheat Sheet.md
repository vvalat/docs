*A (\*) indicates a custom mapping or a plugin action*

### Usual actions ###

| Action                                 |        Key mapping       |
|----------------------------------------|:------------------------:|
|Modes (Normal/CLI/Insert/Replace)       | ESC  /  :  /  i  /  R    |
|Visual Modes (Block/Line/Cursor)        | CTRL-v  /  SHIFT-v  /  v |
|Quit without saving                     | :q                       |
|Quit                                    | :wq                      |
|Save                                    | :w                       |
|Change current line / word under cursor | c  /  ciw                |
|Copy (yank) / current line              | y  /  yy                 |
|Cut (delete) / current line             | d  /  dd                 |
|Paste                                   | p                        |
|Indent: Auto / Increase / Decrease      | =  /  >  /  <            |
|  \\__ Apply to current line            | ==  /  >>  /  <<         |
|  \\__ Autoindent file                  | gg=G                     |
|Undo / Redo                             | u  /  CTRL-r             |
|Find {string}                           | /{string}                |
|Find next / previous                    | n  /  N                  |
|Find next similar word under cursor     | *                        |
|Count occurrences of {word}             | :%s/{word}//gn           |
|Replace {a} with {b}                    | :%s/{a}/{b}/g[params]    |
|  \\__ params:                          |                          |
|    \\________ i: case insensitive      |                          |
|    \\________ c: ask confirmation      |                          |
|Delete all characters until EOL         | D                        |
|Go to beginning / end of word           | b  /  e                  |
|Delete the next {n} words               | [b]d{n}w                 |
|Delete from cursor until {char}         | dt{char}                 |
|Go to beginning / end of line           | 0  /  $                  |
|Go to first line / last line            | gg  /  G                 |
|Go to line #{nb}                        | {nb} G                   |
|Select from cursor to EOL               | v$                       |
|Select from cursor to {char}            | vf{char}                 |
|Move line/selection up/down (*)         | CTRL-Up  /  CTRL-Down    |
|Open {file} in a new tab                | :tabe {file}             |
|Open {file} in a horiz. / vert. pane    | :sp  {file} /  :vs {file}|
|Swith buffer                            | CTRL-w-w                 |
|Navigate between tabs (*)               | CTRL-Left  /  CTRL-Right |
|Call tree navigator (*)                 | F2                       |
|Send to background (return with 'fg')   | CTRL-z                   |

### Advanced actions ###

| Action                                      |  Key mapping              |
|---------------------------------------------|:-------------------------:|
|Toggle display of line numbers               | LEADER-l                  |
|Search by regex                              | /                         |
|  \\_ word delimiters: \\<   \\>             |                           |
|  \\_ spaces/tabs: \\s   \\_s (incl \n)      |                           |
|  \\_ groups: \( \)                          |                           |
|Switch case (cursor/word)                    | ~  /  g~iw                |
|Change content between () / {} / [] / '' ... | ci(  /  ci{  /  ci[  / ci'|
|Omni-completion (*)                          | CTRL-Space / CTRL-x-o     |
|Go to definition (*Pymode)                   | LEADER-d                  |
|  \\_Go back (i.e. previous cursor position) | CTRL-o                    |
|Insert breakpoint (*Pymode)                  | LEADER-b                  |
|Navigate classes/methods (*Pymode)           | [[  /  ]]  /  [M  /  ]M   |
|Show documentation (*Pymode)                 | K                         |
|Toggle folding (*)                           | ,f                        |
|Run code in buffer/selection (*)             | LEADER-r                  |
|Comment current line (*)                     | LEADER-cc                 |
|Switch comment on / off (*)                  | LEADER-Space              |
|Comment selection as block (*)               | LEADER-cs                 |
|Comment until EOL (*)                        | LEADER-c$                 |
|Visualize last author (Git)                  | :Gblame                   |

### Useful commands ###

| Action                                |  Command                 |
|---------------------------------------|:------------------------:|
|Set specific {syntax}                  | :set syntax={syntax}     |
|Set indentation at {char nb}           | :set sw={char nb}        |
