* System
|___ sudo
|___ extundelete
|___ grub-customizer
|___ ntfs-3g
|___ dconf-editor
|___ base-devel

* Terminal & Shell
|___ zsh
|___ oh-my-zsh
|___ terminator

* Enhancements
|___ i3lock-fancy
|   \___ nb: \troll-the-trolls-mode (by modifying /usr/bin/i3lock-fancy):
|            * change the initial effect and hue variables with effect=() and hue=()
|            * for confidentiality effect=(-scale 35% -scale WxH\!) where WxH is the total
|              resolution (i.e. 3840x1080 for two fullHD monitors)
|            * create a 1x1 blank png image
|            * replace /usr/share/i3lock-fancy/icons/lock.png with this image
|            * if TEXT is set in the script insert <TEXT=""> after it has been set,
|              else call i3lock with <-t ''> if supported
|            * bind /usr/bin/i3lock-fancy to some keyboard combo (super + L...)
|___ tree
|___ trash-cli
|___ gnome-tweak-tool

* Tools
|___ veracrypt
|___ google-chrome
|___ postman
|___ dropbox
|___ gimp
|___ imagemagick
|___ libreoffice
|___ vlc

* Git
|___ git
|___ tig

* IDE
|___ emacs
|___ atom
|___ vim
    \___ vundle


* Deployment tools
|___ ngrok
|___ docker

* Databases tools
|___ SQL
|   \___ mysql
|       \___ phpmyadmin workbench
|
|___ MongoDB
|   \___ mongodb
|       \___ mongoadmin
|
|___ Neo4j
    \___ neo4j

* Development tools
|___ Python
|   \___ python>=3.7 virtualenv virtualenvwrapper
|       \___ pip
|           \___ ipython | jupyter rise
|            \___ pyflakes flake8 pylint
|
|___ PHP
|   \___ php7
|       \___ composer
|
|___ Front-end
|   \___ nodejs
|    \___ npm
|     \___ bower
|         \___ jquery
|
|___ Ruby
|   \___ ruby
|       \___ rake
|
|___ C
    \___ gcc
        \___ gdb valgrind
